import numpy as np


def get_standard_dev_character_count(df, sd_threshold=2):
    """
    Create dictionary with character count metrics including list of files that deviate significantly from
    character count mean for each sourceKey
    :param df: dataframe with fileMD5, sourceKey and value columns
    :param sd_threshold: int - threshold for standard deviation
    :return: dictionary object with character count metrics from the dataframe
    """
    source_keys = set(df['sourceKey'])

    character_count = {'num_deviations_away': {}, 'standard_deviation': {}, 'mean': {}, 'min': {}, 'max': {}, 'warning': {}}

    for source_key in source_keys:
        # Filter df to one source key and get character count for all values assigned to that key
        df_temp = df[df['sourceKey'] == source_key]
        df_temp['character_count'] = [len(str(value))for value in df_temp['value']]

        # Get max, min, mean and standard deviation of character counts
        character_count['max'][source_key] = np.max(df_temp['character_count'])
        character_count['min'][source_key] = np.min(df_temp['character_count'])

        mean = np.mean(df_temp['character_count'])
        sd = np.std(df_temp['character_count'])
        character_count['mean'][source_key] = mean
        character_count['standard_deviation'][source_key] = sd

        # For each row, calculate how many standard deviations away from the mean for character count
        character_count['num_deviations_away'][source_key] = {md5: (value-mean)/sd
                                                              for md5, value in zip(df_temp['fileMD5'], df_temp['character_count'])}

        # Get files that are more than {sd_threshold} standard deviations away from mean
        character_count['warning'][source_key] = set([md5 for md5, dev in character_count['num_deviations_away'][source_key].items()
                                                      if abs(dev) > sd_threshold])

    return character_count
