import os
import pandas as pd
import numpy as np
import xml.etree.ElementTree as et
import helpers

# Input
xml_file_dir = "//maia/h$/DataHelix_QUEUE/OUTPUT/NFX/00063/10300/"
single_results_path = 'results/data_single.csv'
table_results_path = 'results/data_table.csv'
output_report_path = 'results/report.txt'

character_count_threshold = 2       # Standard deviation threshold for character count

data_single = []
data_table = []
no_data_files = []

# Read XML files to dataframe
for xml_file_name in os.listdir(xml_file_dir):
    count = 0
    xml_file_path = os.path.join(xml_file_dir, xml_file_name)

    tree = et.parse(xml_file_path)
    root = tree.getroot()

    file_md5 = (os.path.basename(xml_file_path)).split('.')[0]

    for elements in root.iter('ELEMENTS'):
        for element in elements.iter('ELEMENT'):
            if element.attrib['type'] == 'single':
                for field in element.iter('FIELD'):
                    data_single.append(
                        {'fileMD5': file_md5, 'pageNumber': elements.attrib['pageNumber'],
                         'elementId': element.attrib['id'], 'sourceKey': field.attrib['name'].upper().replace(' ', ''),
                         'value': field.text})
                    count += 1

            elif element.attrib['type'] == 'table':
                for table in element.iter('TABLE'):
                    for row in table.iter('TABLE_ROW'):
                        column_count = len(list(row.iter('TABLE_ROW_FIELD')))
                        for field in row.iter('TABLE_ROW_FIELD'):
                            data_table.append(
                                {'fileMD5': file_md5, 'pageNumber': elements.attrib['pageNumber'],
                                 'elementId': element.attrib['id'], 'tableName': table.attrib['table_name'],
                                 'columnCount': column_count, 'rowId': row.attrib['id'],
                                 'sourceKey': field.attrib['name'].upper().replace(' ', ''), 'value': field.text})
                            count += 1
    if count == 0:
        no_data_files.append(file_md5)

df_data_single = pd.DataFrame(data_single)
df_data_table = pd.DataFrame(data_table)

df_data_single.to_csv(single_results_path, encoding='utf-8', index=False)
df_data_table.to_csv(table_results_path, encoding='utf-8', index=False)

# Analysis of missing data points and tables
print('No data extracted from', len(no_data_files), 'files:\n', '\n'.join(no_data_files), file=open(output_report_path, 'w'))

file_list = set(df_data_single['fileMD5']) | set(df_data_table['fileMD5'])
print('\nNumber of files in cluster with extracted data:', len(file_list), file=open(output_report_path, 'a'))

# Check for files missing keys
single_fields_list = set(df_data_single['sourceKey'])
files_missing_data = {}

for key in single_fields_list:
    df_data_single_temp = df_data_single[df_data_single['sourceKey'] == key]
    missing_key_file_list = set(file_list) - set(df_data_single_temp['fileMD5'])
    files_missing_data[key] = missing_key_file_list

    print('\nThe following files are missing sourceKey {', key, '}:\n', '\n'.join(missing_key_file_list),
          file=open(output_report_path, 'a'))

# Check for files missing tables
tables_list = set(df_data_table['tableName'])
files_missing_tables = {}

for table in tables_list:
    df_data_table_temp = df_data_table[df_data_table['tableName'] == table]
    missing_table_file_list = set(file_list) - set(df_data_table_temp['fileMD5'])
    files_missing_tables[table] = missing_table_file_list

    print('\nThe following files are missing sourceTable {', table, '}:\n', '\n'.join(missing_table_file_list),
          file=open(output_report_path, 'a'))

# Check for number of columns in tables
files_diff_col_count = {}

for table in tables_list:
    df_data_table_temp = df_data_table[df_data_table['tableName'] == table]

    column_count = np.argmax(np.bincount(df_data_table_temp['columnCount']))

    df_data_table_temp = df_data_table_temp[df_data_table_temp['columnCount'] != column_count]
    files_diff_col_count[table] = set(df_data_table_temp['fileMD5'])

    print('\nThe following files have more or less columns than expected for sourceTable {', table, '}:\n',
          '\n'.join(set(df_data_table_temp['fileMD5'])),
          file=open(output_report_path, 'a'))

# Get metrics of character count for each key that is not part of a table
character_count_dict = {'single': helpers.get_standard_dev_character_count(df_data_single, character_count_threshold)}

for key in character_count_dict['single']['warning']:
    print('\nThe character count for sourceKey {', key,
          '} in the following files is more than 2 standard deviations away from expected count:\n',
          '\n'.join(character_count_dict['single']['warning'][key]),
          file=open(output_report_path, 'a'))

# Loop through tables and get metrics of character count for each key in that table
for table in tables_list:
    df_data_table_temp = df_data_table[df_data_table['tableName'] == table]
    character_count_dict[table] = helpers.get_standard_dev_character_count(df_data_table_temp, character_count_threshold)

    for key in character_count_dict[table]['warning']:
        print('\nThe character count for sourceKey {', key, '} in table {', table,
              '} in the following files is more than 2 standard deviations away from expected count:\n',
              '\n'.join(character_count_dict[table]['warning'][key]),
              file=open(output_report_path, 'a'))
